package core;

import java.net.*;
import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * A subclass of DatagramSocket which contains methods for sending and receiving
 * messages
 */
public class MyDatagramSocket extends DatagramSocket
{

    private static final int MAX_LEN = 3;

    public MyDatagramSocket(int portNo) throws SocketException
    {
        super(portNo);
    }

    public void sendMessage(InetAddress receiverHost, int receiverPort, byte[] message) throws IOException
    {
        DatagramPacket datagram = new DatagramPacket(message, message.length, receiverHost, receiverPort);
        this.send(datagram);
    } // end sendMessage

    public DatagramPacket receiveMessage() throws IOException
    {
        byte receiveBuffer[] = new byte[MAX_LEN];

        DatagramPacket datagram = new DatagramPacket(receiveBuffer, MAX_LEN);
        this.receive(datagram);
        return datagram;
    } //end receiveMessage
} //end class
