/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import java.net.DatagramPacket;
import core.MyDatagramSocket;
import java.awt.List;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ronan
 */
class ACKListener extends Thread
{

    private MyDatagramSocket socket;
    private byte highest_ordered_packet;
    private int unacked = 0;
    
    public int get_unacked()
    {
        return unacked;
    }
    
    public void increase_unacked()
    {
        unacked++;
    }
    public void decrease_unacked()
    {
        unacked--;
    }
    

    public ACKListener(MyDatagramSocket socket) throws SocketException
    {
        this.socket = socket;
    }
    public byte getHighest()
    {
        return this.highest_ordered_packet;
    }
    public TreeSet<Byte> ack_number_list = new TreeSet<>();

    @Override
    public void run()
    {
        
       

        while (true)
        {
            //listen for packet 
            DatagramPacket ack;
            try
            {
                
                ack = socket.receiveMessage();
                
                
                unacked = ack.getData()[0] - highest_ordered_packet;
               
                //byte last_num = 0;
                
               
                highest_ordered_packet = ack.getData()[0];
                
                //for(byte b : ack_number_list)
                //{  
                    
                    
                    //if(b == last_num+(byte)1)
                    //{
                    //    temp = b;
                    //}
                    //else
                    //{
                    //    break;
                    //}
                    //last_num = b;
                    
                //}
                
               
               
                
                
            } catch (IOException ex)
            {
                Logger.getLogger(ACKListener.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }
}
