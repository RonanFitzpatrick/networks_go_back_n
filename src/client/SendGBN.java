package client;

import java.io.*;
import java.net.*;
import core.MyDatagramSocket;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SendGBN extends Thread
{

    private final byte N = 10;              //the N from "Go-Back-N"
    private final byte MAX_NUMBER = 20; //The maximum sequence number        
    private final int DELAY = 1000;         //1 second delay for timeout
    private byte packet_number = 1;
    private ACKListener listener;
    
    private ArrayList<Unacked_packet> list;
    //the socket
    MyDatagramSocket socket;
    InetAddress ip;

    //variables from the algorithm
    byte base = 1;
    byte next_Number = 1;   //Pay attention to this

    //needed for receiver thread
    boolean stop = false;
    volatile boolean done = false;

    //number of packets sent, but unacknowledged
    int unackedknowledged_packets = 0;

    public void decrease_unacked()
    {
        unackedknowledged_packets--;
    }

    public SendGBN(InetAddress ip, MyDatagramSocket socket) throws SocketException
    {
        this.ip = ip;
        this.socket = socket;
    }

    //constructor
    @Override
    public void run()
    {

        try
        {
            list = new ArrayList();
            listener = new ACKListener(socket);
            listener.start();
            processUserData();

        } catch (IOException ex)
        {
        }

    }

    public void processUserData() throws IOException
    {
        //create input reading mechanism
        BufferedReader in
                = new BufferedReader(new InputStreamReader(System.in));

        

       
        ArrayList<String> wordlist = new ArrayList();
        wordlist.add("hi ");
        wordlist.add("hey ");
        wordlist.add("sent ");
       
      
        for(String line:wordlist)
        {

            //send one packet per character
            if (!stop)
            {
                for (int i = 0; i < line.length(); i++)
                {

                    char c = line.charAt(i);
                    //wait until the queue is no longer full
                   
                    while (listener.get_unacked() == N)
                    {
                        //we can do this because we will be running other threads
                        //we'll give them a chance to run
                        
                        
                    }

                    sendPacket(c, packet_number);
                    listener.increase_unacked();
                     

                    list.add(new Unacked_packet(packet_number, c));
                    packet_number++;
                    unackedknowledged_packets++;
                   
                   
                }

            }

            ///////////////////////////////////////////
            //
            // wait until the queue is no longer full
            // send an end of line character
            //
        }
        done = true;
       
        
    }

    /* 
     * The following three methods are defined as synchronized so that only one can be active
     * at a time from any given thread (main thread, ackListener or timer).
     * This prevents problems caused by multiple threads updating your 
     * data structures at the same time. You do NOT want to have anything
     * in these methods that blocks the thread such as a socket.receive or
     * a reader.readLine, because it will prevent other threads from entering
     * their methods
     */
    
    public void start_timer(byte pNum)
    {
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask()
                    {

                        byte num = pNum;

                        @Override
                        public void run()
                        {
                            
                            if (num >= listener.getHighest() && !stop)
                            {

                                try
                                {
                                    
                                    stop = true;
                                   
                                    int highest = listener.getHighest();
                                    
                                    for (int i = highest; i < list.size(); i++)
                                    {
                                        
                                        sendPacket(list.get(i).getContent(), list.get(i).getPacket_number());
                                        start_timer(list.get(i).getPacket_number());
                                        
                                    }
                                    stop = false;
                                } catch (IOException ex)
                                {
                                    Logger.getLogger(SendGBN.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            
                                cancel();
                                
                            

                        }
                    },2000);
                               
        
    }
    
    
    
  
    
   
    public synchronized void sendPacket(char c, byte number) throws IOException
    {
        ///////////////////////////////////////////
        //
        // Turn the char c into a packet and send it off.
        // Remember that a character is 2 bytes.
        // Called from processUserData().
        //
        //
        
        start_timer(number);
        byte lo = (byte) (c & 0xFF);
        byte hi = (byte) ((c >> 8) & 0xFF);

        byte[] array =
        {
            (number), hi, lo
        };
        socket.sendMessage(ip, 7609, array);

    }
    
    

}
