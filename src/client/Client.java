package client;


import java.io.*;
import java.net.*;
import core.MyDatagramSocket;


public class Client
{

    private static final int PORT = 8000;
    private static SendGBN sender;
    public static void main(String[] args) throws IOException
    {

        System.out.println();
        InetAddress ipAddress = null;
        if (args.length > 0)
        {
            //get specified host
            ipAddress = InetAddress.getByName(args[0]);
        } else
        {
            //use default local host
            ipAddress = InetAddress.getByName("localhost"); //loopback
        }
        //create a new sender
        MyDatagramSocket socket = new MyDatagramSocket(PORT);
        sender = new SendGBN(ipAddress, socket);
        sender.start();
       

        //start processing the user's data
        
    }
    
    public static Thread getThread()
    {
        return sender;
    }

}
